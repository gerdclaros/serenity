package bdd.template.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;


public class ProfilePage {

    public static Target GET_ACCOUNT_TYPE=
            Target.the("Account Type").located(By.cssSelector("input.form-control[placeholder='Account Type']"));

    public static Target FIRST_NAME_VALUE =
            Target.the("The first name value").located(By.cssSelector("input.form-control[placeholder='First name']"));
}
