package bdd.template.ui;

import org.openqa.selenium.By;
import net.serenitybdd.screenplay.targets.Target;

public class LoginFormPage {


    public static Target EMAIL= Target.the("the email field").located(By.cssSelector("input[name='email'][type='text']"));
    public static Target PASSWORD= Target.the("the password field").located
            (By.cssSelector("input[name='password']"));
}
