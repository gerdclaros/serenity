package bdd.template.questions;

import bdd.template.ui.ProfilePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Value;

public class AccountType implements Question <String>{

    @Override
    public String answeredBy(Actor actor){
        return Value.of(ProfilePage.GET_ACCOUNT_TYPE).viewedBy(actor).asString();
    }

}
