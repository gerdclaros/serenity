package bdd.template.questions;

import net.serenitybdd.screenplay.Question;

public class TheProfile {

    public static Question<String> accountType(){
        return new AccountType();
    }
    public static Question <String> firstName(){
        return new FirstName();
    }
}
