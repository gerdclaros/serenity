package bdd.template.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Value;
import bdd.template.ui.ProfilePage;


public class FirstName implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Value.of(ProfilePage.FIRST_NAME_VALUE).viewedBy(actor).asString();
    }
}
