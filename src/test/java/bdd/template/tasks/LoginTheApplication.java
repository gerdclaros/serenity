package bdd.template.tasks;

import bdd.template.ui.LoginFormPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LoginTheApplication implements Task {

    private final String email;
    private final String password;

    public LoginTheApplication(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public static LoginTheApplication  withTheCredentials(String email, String password) {
        return instrumented(LoginTheApplication.class,email,password);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(email).into(LoginFormPage.EMAIL),
                Enter.theValue(password).into(LoginFormPage.PASSWORD).thenHit(Keys.ENTER)
        );
    }

}
