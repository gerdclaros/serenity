package bdd.template.tasks;

import bdd.template.ui.DashBoardPage;
import bdd.template.ui.LoginFormPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class Access implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(DashBoardPage.PROFILE_LINK));
    }
    public static Access theUserProfile(){
        return new Access();
    }

}
