package bdd.template.tasks;

import bdd.template.ui.ApplicationHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

public class OpenTheBrowser implements Task {
    ApplicationHomePage applicationHomePage;

    public static OpenTheBrowser onTheApplicationPage() {
        return new OpenTheBrowser();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Open.browserOn().the(applicationHomePage));
    }


}
