package bdd.template.features.login;

import bdd.template.questions.TheProfile;
import bdd.template.tasks.Access;
import bdd.template.tasks.LoginTheApplication;
import bdd.template.tasks.OpenTheBrowser;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.CoreMatchers.is;
import static net.serenitybdd.screenplay.GivenWhenThen.*;

@RunWith(SerenityRunner.class)
public class LoginStory {
    private Actor miguel = Actor.named("Miguel");

    @Managed(uniqueSession = true)
    private WebDriver hisBrower;

    @Before
    public void miguelCanBrowserTheWeb() {
        miguel.can(BrowseTheWeb.with(hisBrower));
    }

    @Test
    public void shoul_be_able_to_login_as_administrator() {
        givenThat(miguel).wasAbleTo(OpenTheBrowser.onTheApplicationPage());
        and(miguel).wasAbleTo(LoginTheApplication.withTheCredentials("admin@phptravels.com", "demoadmin"));
        when(miguel).attemptsTo(Access.theUserProfile());
        then(miguel).should(seeThat(TheProfile.accountType(), is("Super Admin")));
        and(miguel).should(seeThat(TheProfile.firstName(),is("Super Admin1")));

    }

}
